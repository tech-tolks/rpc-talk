## How to import Postman collection

1. Choose "Collections" tab.
2. Click import button.
   ![Postman_1.png](../resources/img/Postman_1.png)
3. Click "Upload Files" button.
   ![Postman_2.png](../resources/img/Postman_2.png)
4. Choose "gRPC talk.postman_collection.json" from project resources/postman folder and click "Upload" button.
   ![Postman_3.png](../resources/img/Postman_3.png)
5. Click "Import" button.
   ![Postman_4.png](../resources/img/Postman_4.png)
6. Now collection should be visible on "Collections" tab.
   ![Postman_5.png](../resources/img/Postman_5.png)

## How to import Postman collection environment variables

1. Choose "Environments" tab.
2. Click import button.
   ![Postman_6.png](../resources/img/Postman_6.png)
3. Click "Upload Files" button.
   ![Postman_7.png](../resources/img/Postman_7.png)
4. Choose "rpc-talk.postman_environment.json" from project resources/postman folder and click "Upload" button.
   ![Postman_3.png](../resources/img/Postman_8.png)
5. Click "Import" button.
   ![Postman_9.png](../resources/img/Postman_9.png)
6. Now environments should be visible on "Environments" tab. Now change your client_port and server_port for app-client
   rest api and app-server gRPC endpoints accordingly, or left it as is if you use default port binding.
   ![Postman_10.png](../resources/img/Postman_10.png)
7. Make sure you use environment variables.
   ![Postman_11.png](../resources/img/Postman_11.png)
