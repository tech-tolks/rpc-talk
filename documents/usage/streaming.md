# Streaming

## Client-Side

![Streaming_1.png](../resources/img/Streaming_1.png)

**"Add users gRPC Client-Side streaming" request from Postman collection**

**GET [/user/grpc/add/stream?numberOfUsers=10]()**

## Server-Side

![Streaming_2.png](../resources/img/Streaming_2.png)

**"Get users gRPC Server-Side streaming" request from Postman collection**

**GET [/user/grpc/get/stream?numberOfUsers=10]()**

## Bidirectional

![Streaming_3.png](../resources/img/Streaming_3.png)

**"Add users gRPC Bidirectional streaming" request from Postman collection**

**GET [/user/grpc/add-and-receive/stream?numberOfUsers=10]()**