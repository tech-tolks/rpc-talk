# Basic

## app-server gRPC call

**"Greet gRPC server" request from Postman collection**

**GET [/greeting/grpc?name=Jenkins]()**

![Diagram_1.png](../resources/img/Diagram_1.png)

1. Send request to app-client REST greeting endpoint.
2. app-client service call app-server using gRPC client.
3. app-server sends gRPC response back to the app-client.
4. app-client sends response from app-server to the Postman/Swagger client.

**Logs:**

**Request:**

```shell
INBOUND DATA: 
streamId=7 
padding=0 
endStream=true 
length=14 
bytes=00000000090a074a656e6b696e73 // binary request body. Original string - Jenkins
```

**Response:**

```shell
OUTBOUND DATA: 
streamId=7 
padding=0 
endStream=false 
length=21 
bytes=00000000100a0e48656c6c6f2c204a656e6b696e73 // binary response body. Original string - Hello, Jenkins
```

## app-server REST call

**"Greet REST server" request from Postman collection**

**GET [/greeting/rest?name=Jenkins]()**

![Diagram_2.png](../resources/img/Diagram_2.png)

1. Send request to app-client REST greeting endpoint.
2. app-client service call app-server using REST client.
3. app-server sends REST response back to the app-client.
4. app-client sends response from app-server to the Postman/Swagger client.