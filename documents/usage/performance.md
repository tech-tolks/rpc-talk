# Performance

### app-server gRPC call

**"Add users gRPC server" request from Postman collection**

**GET [/user/grpc/add?numberOfUsers=10]()**

![Diagram_3.png](../resources/img/Diagram_3.png)

1. Send request to app-client REST user endpoint.
2. app-client generates 10 users.
3. app-client service call app-server using gRPC client.
4. app-server sends gRPC response back to the app-client.
5. app-client sends response from app-server to the Postman/Swagger client.

### app-server REST call

**"Add users REST server" request from Postman collection**

**GET [/user/rest/add?numberOfUsers=10]()**

![Diagram_4.png](../resources/img/Diagram_4.png)

1. Send request to app-client REST user endpoint.
2. app-client generates 10 users.
3. app-client service call app-server using REST client.
4. app-server sends REST response back to the app-client.
5. app-client sends response from app-server to the Postman/Swagger client.

### Response

```json5
{
  "requestPayloadSize": {
    // bytes
    "by": 2621.0,
    // kilobytes
    "kb": 2.5595703125,
    // megabytes
    "mb": 0.0024995803833007812
  },
  "responseTime": {
    // milliseconds
    "ms": 41.9277,
    // seconds
    "sec": 0.0419277
  },
  // original response from app-server
  "message": 10
}
```

### Slow connection simulation

The following commands will reduce upload and download speed to **3 mb/sec** with **13 ms** delay.

```shell
$ sudo tc qdisc add dev lo root handle 1: htb default 12 
$ sudo tc class add dev lo parent 1:1 classid 1:12 htb rate 3000kbps ceil 3000kbps 
$ sudo tc qdisc add dev lo parent 1:12 netem delay 13ms
```

You can choose right rtt according to the following table, provided by Amazon

![inter_region_latency.png](../resources/img/inter_region_latency.png)

Then you can perform steps before with different number of users. For example 1_000_000 users ~ 250mb payload size for
REST endpoint.

Use the following command to stop throttling:

```shell
sudo tc qdisc del dev lo root
```

### Performance comparison

**gRPC**

```json5
{
  "requestPayloadSize": {
    "by": 1.95E8,
    "kb": 190429.6875,
    "mb": 185.96649169921875
  },
  "responseTime": {
    "ms": 67691.706015,
    "sec": 67.691706015
  },
  "message": 1000000
}
```

**REST**

```json5
{
  "requestPayloadSize": {
    "by": 2.62000001E8,
    "kb": 255859.3759765625,
    "mb": 249.86267185211182
  },
  "responseTime": {
    "ms": 120978.301588,
    "sec": 120.978301588
  },
  "message": 1000000
}
```

As you can see gRPC is faster and dramatically decrease payload size.