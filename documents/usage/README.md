# Showcase

app-client service has REST endpoints that can be used to call app-server REST and gRPC endpoints. Please, visit
the [Postman document](../postman/README.md) to import project Postman collection.

## [Basic](./basic.md)

## [Performance](./performance.md)

## [Streaming](./streaming.md)
