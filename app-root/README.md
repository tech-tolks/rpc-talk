# app-root

### About

Contains [BOM](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html#bill-of-materials-bom-poms)
file with common dependencies for app-client and app-server services.

See [pom.xml](./pom.xml) for more details about common dependencies.

### How to build

```shell
./mvnw clean install
```