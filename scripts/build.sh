#!/bin/sh

./mvnw clean install -f ./app-root &&
./mvnw clean install -f ./app-core &&
./mvnw clean install -f ./app-server &&
./mvnw clean install -f ./app-client
