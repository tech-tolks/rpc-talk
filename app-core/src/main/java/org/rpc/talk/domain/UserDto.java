package org.rpc.talk.domain;

import org.rpc.talk.grpc.service.User;


public class UserDto {

    private String firstName;
    private String lastName;
    private String email;
    private AddressDto addressDto;


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public AddressDto getAddressDto() {
        return addressDto;
    }


    public void setAddressDto(AddressDto addressDto) {
        this.addressDto = addressDto;
    }


    public User toGrpcUser() {
        return User.newBuilder()
                .setFirstName(getFirstName())
                .setLastName(getLastName())
                .setEmail(getEmail())
                .setAddress(addressDto.toGrpcAddress())
                .build();
    }
}
