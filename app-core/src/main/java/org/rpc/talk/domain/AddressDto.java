package org.rpc.talk.domain;

import org.rpc.talk.grpc.service.User;


public class AddressDto {

    private String zipCode;
    private String street;


    public String getZipCode() {
        return zipCode;
    }


    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }


    public String getStreet() {
        return street;
    }


    public void setStreet(String street) {
        this.street = street;
    }


    public User.Address toGrpcAddress() {
        return User.Address.newBuilder()
                .setStreet(getStreet())
                .setZipCode(getZipCode())
                .build();
    }
}
