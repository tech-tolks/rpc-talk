# app-core

### About

Lib with common classes and proto files for app-client and app-server

### Why need to share proto files as part of a lib or repository

Both client and server need to use the same proto files source to avoid mismatches in communication. That means if you
make changes in proto files, client and server must regenerate it again.

### How to build

```shell
./mvnw clean install
```

This command will compile Java sources and generate classes from *.proto files