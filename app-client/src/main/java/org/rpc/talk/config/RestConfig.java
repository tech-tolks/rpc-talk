package org.rpc.talk.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;


@Configuration
public class RestConfig {

    @Bean
    public RestTemplate restTemplate(@Value("${app.server.rest.root-uri}") final String rootUri) {
        return new RestTemplateBuilder()
                .rootUri(rootUri)
                .additionalInterceptors(new RestTemplateHeaderModifierInterceptor())
                .build();
    }


    public static class RestTemplateHeaderModifierInterceptor implements ClientHttpRequestInterceptor {

        public static final String REQUEST_CONTENT_LENGTH_HEADER = "REQUEST_CONTENT_LENGTH";
        public static final String REQUEST_EXECUTION_TIME_HEADER = "REQUEST_EXECUTION_TIME";


        @Override
        public ClientHttpResponse intercept(final HttpRequest request, final byte[] body, final ClientHttpRequestExecution execution) throws IOException {
            long startTime = System.nanoTime();
            final ClientHttpResponse response = execution.execute(request, body);
            long endTime = System.nanoTime();
            long duration = (endTime - startTime);

            response.getHeaders().add(REQUEST_CONTENT_LENGTH_HEADER, Long.toString(request.getHeaders().getContentLength()));
            response.getHeaders().add(REQUEST_EXECUTION_TIME_HEADER, Long.toString(duration));
            return response;
        }
    }
}
