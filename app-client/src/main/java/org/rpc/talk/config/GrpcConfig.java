package org.rpc.talk.config;

import io.grpc.ManagedChannel;
import io.grpc.netty.NettyChannelBuilder;
import org.rpc.talk.grpc.service.GreetingServiceGrpc;
import org.rpc.talk.grpc.service.UserServiceGrpc;
import org.rpc.talk.grpc.service.UserStreamServiceGrpc;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class GrpcConfig {

    @Bean
    public ManagedChannel channel(
            @Value("${app.server.host}") String host,
            @Value("${app.server.grpc.port}") Integer port
    ) {
        return NettyChannelBuilder.forAddress(host, port)
                .usePlaintext()
                .build();
    }


    @Bean
    public GreetingServiceGrpc.GreetingServiceBlockingStub greetingServiceBlockingStub(final ManagedChannel channel) {
        return GreetingServiceGrpc.newBlockingStub(channel);
    }


    @Bean
    public UserServiceGrpc.UserServiceBlockingStub userServiceBlockingStub(final ManagedChannel channel) {
        return UserServiceGrpc.newBlockingStub(channel);
    }


    @Bean
    public UserStreamServiceGrpc.UserStreamServiceStub userStreamServiceStub(final ManagedChannel channel) {
        return UserStreamServiceGrpc.newStub(channel);
    }


    @Bean
    public UserStreamServiceGrpc.UserStreamServiceBlockingStub userStreamServiceBlockingStub(final ManagedChannel channel) {
        return UserStreamServiceGrpc.newBlockingStub(channel);
    }
}
