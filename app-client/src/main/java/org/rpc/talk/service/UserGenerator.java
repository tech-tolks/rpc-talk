package org.rpc.talk.service;

import org.rpc.talk.domain.AddressDto;
import org.rpc.talk.domain.UserDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


@Service
public class UserGenerator {

    public Stream<UserDto> generateUsersStream(final Integer numberOfUsers) {
        return IntStream.range(0, numberOfUsers)
                .mapToObj(i -> generateUser());
    }


    public List<UserDto> generateUsers(final Integer numberOfUsers) {
        return IntStream.range(0, numberOfUsers)
                .mapToObj(i -> generateUser())
                .collect(Collectors.toList());
    }


    private UserDto generateUser() {
        final UserDto user = new UserDto();
        user.setFirstName(UUID.randomUUID().toString());
        user.setLastName(UUID.randomUUID().toString());
        user.setEmail(UUID.randomUUID().toString());
        user.setAddressDto(generateAddress());
        return user;
    }


    private AddressDto generateAddress() {
        final AddressDto address = new AddressDto();
        address.setStreet(UUID.randomUUID().toString());
        address.setZipCode(UUID.randomUUID().toString());
        return address;
    }
}
