package org.rpc.talk.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


public class ResponseTime {

    private final double ms;
    private final double sec;


    public ResponseTime(final double responseTime) {
        this.ms = responseTime / 1000000L;
        this.sec = responseTime / 1000000000L;
    }


    public double getMs() {
        return ms;
    }


    public double getSec() {
        return sec;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
