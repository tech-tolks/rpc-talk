package org.rpc.talk.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.http.ResponseEntity;

import static org.rpc.talk.config.RestConfig.RestTemplateHeaderModifierInterceptor.REQUEST_CONTENT_LENGTH_HEADER;
import static org.rpc.talk.config.RestConfig.RestTemplateHeaderModifierInterceptor.REQUEST_EXECUTION_TIME_HEADER;


public class ResponseStat<T> {

    private final PayloadSize requestPayloadSize;
    private final ResponseTime responseTime;
    private final T message;


    public ResponseStat(final ResponseEntity<T> response) {
        final String requestPayloadSize = response.getHeaders().get(REQUEST_CONTENT_LENGTH_HEADER).get(0);
        final String responseTime = response.getHeaders().get(REQUEST_EXECUTION_TIME_HEADER).get(0);

        this.requestPayloadSize = new PayloadSize(Double.parseDouble(requestPayloadSize));
        this.responseTime = new ResponseTime(Double.parseDouble(responseTime));
        this.message = response.getBody();
    }


    public ResponseStat(final int requestPayloadSize,
                        final long responseTime,
                        final T message) {
        this.requestPayloadSize = new PayloadSize(requestPayloadSize);
        this.responseTime = new ResponseTime(responseTime);
        this.message = message;
    }


    public PayloadSize getRequestPayloadSize() {
        return requestPayloadSize;
    }


    public ResponseTime getResponseTime() {
        return responseTime;
    }


    public T getMessage() {
        return message;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
