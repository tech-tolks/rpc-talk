package org.rpc.talk.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


public class PayloadSize {

    private final double by;
    private final double kb;
    private final double mb;


    public PayloadSize(final double size) {
        this.by = size;
        this.kb = size / 1024;
        this.mb = kb / 1024;
    }


    public double getBy() {
        return by;
    }


    public double getKb() {
        return kb;
    }


    public double getMb() {
        return mb;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
