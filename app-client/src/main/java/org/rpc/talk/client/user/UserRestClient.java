package org.rpc.talk.client.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.domain.ResponseStat;
import org.rpc.talk.domain.UserDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@Service
public class UserRestClient {

    private static final Logger LOGGER = LogManager.getLogger(UserRestClient.class);

    private static final String URI = "/user";

    private final RestTemplate restTemplate;


    public UserRestClient(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    public ResponseStat<Integer> add(final List<UserDto> users) {
        LOGGER.debug("Add {} users REST request.", users.size());

        final HttpEntity<List<UserDto>> request = new HttpEntity<>(users);
        final ResponseEntity<Integer> response = restTemplate.exchange(URI, HttpMethod.POST, request, Integer.class);
        final ResponseStat<Integer> responseStat = new ResponseStat<>(response);

        LOGGER.debug("Add users REST response: {}, client result: {}", response, responseStat);

        return responseStat;
    }
}
