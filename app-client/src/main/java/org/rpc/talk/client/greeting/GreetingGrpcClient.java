package org.rpc.talk.client.greeting;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.grpc.service.GreetingRequest;
import org.rpc.talk.grpc.service.GreetingResponse;
import org.rpc.talk.grpc.service.GreetingServiceGrpc;
import org.springframework.stereotype.Service;


@Service
public class GreetingGrpcClient {

    private static final Logger LOGGER = LogManager.getLogger(GreetingGrpcClient.class);

    private final GreetingServiceGrpc.GreetingServiceBlockingStub greetingServiceBlockingStub;


    public GreetingGrpcClient(final GreetingServiceGrpc.GreetingServiceBlockingStub greetingServiceBlockingStub) {
        this.greetingServiceBlockingStub = greetingServiceBlockingStub;
    }


    public String greet(final String name) {
        final GreetingRequest request = GreetingRequest.newBuilder()
                .setName(name)
                .build();

        LOGGER.debug("Greeting gRPC request: {}", request);

        final GreetingResponse response = greetingServiceBlockingStub.hello(request);

        LOGGER.debug("Greeting gRPC response: {}", response);

        return response.getGreeting();
    }
}