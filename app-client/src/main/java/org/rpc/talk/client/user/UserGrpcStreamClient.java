package org.rpc.talk.client.user;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.domain.UserDto;
import org.rpc.talk.grpc.service.AddUsersResponse;
import org.rpc.talk.grpc.service.GetUsersRequest;
import org.rpc.talk.grpc.service.User;
import org.rpc.talk.grpc.service.UserStreamServiceGrpc;
import org.rpc.talk.service.UserGenerator;
import org.springframework.stereotype.Service;

import java.util.Iterator;


@Service
public class UserGrpcStreamClient {

    private static final Logger LOGGER = LogManager.getLogger(UserGrpcStreamClient.class);

    private final UserStreamServiceGrpc.UserStreamServiceStub userStreamServiceStub;
    private final UserStreamServiceGrpc.UserStreamServiceBlockingStub userStreamServiceBlockingStub;
    private final UserGenerator userGenerator;


    public UserGrpcStreamClient(final UserStreamServiceGrpc.UserStreamServiceStub userStreamServiceStub,
                                final UserStreamServiceGrpc.UserStreamServiceBlockingStub userStreamServiceBlockingStub,
                                final UserGenerator userGenerator) {
        this.userStreamServiceStub = userStreamServiceStub;
        this.userStreamServiceBlockingStub = userStreamServiceBlockingStub;
        this.userGenerator = userGenerator;
    }


    public void add(final Integer numberOfUsers) {
        final StreamObserver<AddUsersResponse> responseObserver = buildAddUsersResponseObserver();
        final StreamObserver<User> requestObserver = userStreamServiceStub.add(responseObserver);

        try {
            userGenerator.generateUsersStream(numberOfUsers)
                    .map(UserDto::toGrpcUser)
                    .forEach(requestObserver::onNext);
        } catch (RuntimeException e) {
            requestObserver.onError(e);
            throw e;
        }

        requestObserver.onCompleted();
    }


    public void addAndReceive(final Integer numberOfUsers) {
        final StreamObserver<AddUsersResponse> responseObserver = buildAddUsersResponseObserver();
        final StreamObserver<User> requestObserver = userStreamServiceStub.addAndReceive(responseObserver);

        try {
            userGenerator.generateUsersStream(numberOfUsers)
                    .map(UserDto::toGrpcUser)
                    .peek(user -> LOGGER.debug("User to add: {}", user.getFirstName()))
                    .forEach(requestObserver::onNext);
        } catch (Exception e) {
            requestObserver.onError(e);
            throw e;
        }

        requestObserver.onCompleted();
    }


    public void get(final Integer numberOfUsers) {
        final GetUsersRequest request = GetUsersRequest.newBuilder()
                .setNumberOfUsers(numberOfUsers)
                .build();

        LOGGER.debug("Try to receive {} users", numberOfUsers);

        try {
            final Iterator<User> users = userStreamServiceBlockingStub.get(request);

            while (users.hasNext()) {
                final User user = users.next();
                LOGGER.debug("Receive user with name: {}", user.getFirstName());
            }
        } catch (StatusRuntimeException e) {
            LOGGER.debug("Unable to get users. Process failed with status: {}", e.getStatus());
        }
    }


    private StreamObserver<AddUsersResponse> buildAddUsersResponseObserver() {
        return new StreamObserver<AddUsersResponse>() {
            @Override
            public void onNext(final AddUsersResponse addUsersResponse) {
                LOGGER.debug("Total number of users: {}", addUsersResponse.getNumberOfUsers());
            }


            @Override
            public void onError(final Throwable throwable) {
                LOGGER.error("Unable to add users. Process failed with status: {}", Status.fromThrowable(throwable));
            }


            @Override
            public void onCompleted() {
                LOGGER.debug("All users added successfully");
            }
        };
    }
}
