package org.rpc.talk.client.greeting;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class GreetingRestClient {

    private static final Logger LOGGER = LogManager.getLogger(GreetingRestClient.class);

    private static final String URI = "/greet?name={q}";

    private final RestTemplate restTemplate;


    public GreetingRestClient(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    public String greet(final String name) {
        LOGGER.debug("Greeting REST request name param: {}", name);

        final String greeting = restTemplate.getForEntity(URI, String.class, name).getBody();

        LOGGER.debug("Greeting REST response: {}", greeting);

        return greeting;
    }
}
