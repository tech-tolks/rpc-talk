package org.rpc.talk.client.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.domain.ResponseStat;
import org.rpc.talk.domain.UserDto;
import org.rpc.talk.grpc.service.AddUsersRequest;
import org.rpc.talk.grpc.service.AddUsersResponse;
import org.rpc.talk.grpc.service.User;
import org.rpc.talk.grpc.service.UserServiceGrpc;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserGrpcClient {

    private static final Logger LOGGER = LogManager.getLogger(UserGrpcClient.class);

    private final UserServiceGrpc.UserServiceBlockingStub userServiceBlockingStub;


    public UserGrpcClient(final UserServiceGrpc.UserServiceBlockingStub userServiceBlockingStub) {
        this.userServiceBlockingStub = userServiceBlockingStub;
    }


    public ResponseStat<Integer> add(final List<UserDto> users) {
        final AddUsersRequest request = AddUsersRequest.newBuilder()
                .addAllUsers(mapUsers(users))
                .build();

        LOGGER.debug("Add {} users gRPC request", request.getUsersCount());

        long start = System.nanoTime();

        final AddUsersResponse response = userServiceBlockingStub.add(request);

        long end = System.nanoTime();
        long duration = (end - start);

        final ResponseStat<Integer> responseStat = new ResponseStat<>(request.getSerializedSize(), duration, response.getNumberOfUsers());

        LOGGER.debug("Add users gRPC response: {}, client result: {}", response, responseStat);

        return responseStat;
    }


    private List<User> mapUsers(final List<UserDto> users) {
        return users.stream()
                .map(UserDto::toGrpcUser)
                .collect(Collectors.toList());
    }
}
