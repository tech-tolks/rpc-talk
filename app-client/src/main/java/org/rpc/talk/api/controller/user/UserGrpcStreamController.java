package org.rpc.talk.api.controller.user;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.client.user.UserGrpcStreamClient;
import org.rpc.talk.domain.ResponseStat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.rpc.talk.api.constant.SwaggerConstants.OK_STATUS;
import static org.rpc.talk.api.constant.SwaggerConstants.SERVER_ERROR;
import static org.rpc.talk.api.constant.SwaggerConstants.SERVER_ERROR_STATUS;
import static org.rpc.talk.api.constant.SwaggerConstants.SUCCESSFUL_OPERATION;
import static org.rpc.talk.api.constant.UserConstants.DEFAULT_NUMBER_OF_USERS_PARAM_VALUE;
import static org.rpc.talk.api.constant.UserConstants.NUMBER_OF_USERS_PARAM;
import static org.rpc.talk.api.constant.UserConstants.USER_ENDPOINT;


@RestController
@RequestMapping(value = USER_ENDPOINT)
@Tag(name = "Users streaming")
public class UserGrpcStreamController {

    private static final Logger LOGGER = LogManager.getLogger(UserGrpcStreamController.class);

    private final UserGrpcStreamClient userGrpcStreamClient;


    public UserGrpcStreamController(final UserGrpcStreamClient userGrpcStreamClient) {
        this.userGrpcStreamClient = userGrpcStreamClient;
    }


    @Operation(
            tags = {"grpc", "add users", "stream"},
            method = "GET",
            parameters = {
                    @Parameter(name = NUMBER_OF_USERS_PARAM, in = ParameterIn.QUERY,
                            content = @Content(schema = @Schema(implementation = String.class, defaultValue = DEFAULT_NUMBER_OF_USERS_PARAM_VALUE)))
            },
            responses = {
                    @ApiResponse(responseCode = OK_STATUS, description = SUCCESSFUL_OPERATION),
                    @ApiResponse(responseCode = SERVER_ERROR_STATUS, description = SERVER_ERROR,
                            content = @Content(schema = @Schema(implementation = Object.class)))
            }
    )
    @GetMapping(value = "/grpc/add/stream", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseStat<Integer>> add(
            @RequestParam(value = NUMBER_OF_USERS_PARAM, required = false, defaultValue = DEFAULT_NUMBER_OF_USERS_PARAM_VALUE) final Integer numberOfUsers
    ) {
        LOGGER.debug("Receive request to add {} users for gRPC stream server call", numberOfUsers);

        userGrpcStreamClient.add(numberOfUsers);

        LOGGER.debug("Finis gRPC stream server call processing");

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @Operation(
            tags = {"grpc", "add and receive users", "stream"},
            method = "GET",
            parameters = {
                    @Parameter(name = NUMBER_OF_USERS_PARAM, in = ParameterIn.QUERY,
                            content = @Content(schema = @Schema(implementation = String.class, defaultValue = DEFAULT_NUMBER_OF_USERS_PARAM_VALUE)))
            },
            responses = {
                    @ApiResponse(responseCode = OK_STATUS, description = SUCCESSFUL_OPERATION),
                    @ApiResponse(responseCode = SERVER_ERROR_STATUS, description = SERVER_ERROR,
                            content = @Content(schema = @Schema(implementation = Object.class)))
            }
    )
    @GetMapping(value = "/grpc/add-and-receive/stream", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseStat<Integer>> addAndReceive(
            @RequestParam(value = NUMBER_OF_USERS_PARAM, required = false, defaultValue = DEFAULT_NUMBER_OF_USERS_PARAM_VALUE) final Integer numberOfUsers
    ) {
        LOGGER.debug("Receive request to add {} users for gRPC stream server call", numberOfUsers);

        userGrpcStreamClient.addAndReceive(numberOfUsers);

        LOGGER.debug("Finis gRPC stream server call processing");

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @Operation(
            tags = {"grpc", "get users", "stream"},
            method = "GET",
            parameters = {
                    @Parameter(name = NUMBER_OF_USERS_PARAM, in = ParameterIn.QUERY,
                            content = @Content(schema = @Schema(implementation = String.class, defaultValue = DEFAULT_NUMBER_OF_USERS_PARAM_VALUE)))
            },
            responses = {
                    @ApiResponse(responseCode = OK_STATUS, description = SUCCESSFUL_OPERATION),
                    @ApiResponse(responseCode = SERVER_ERROR_STATUS, description = SERVER_ERROR,
                            content = @Content(schema = @Schema(implementation = Object.class)))
            }
    )
    @GetMapping(value = "/grpc/get/stream", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseStat<Integer>> get(
            @RequestParam(value = NUMBER_OF_USERS_PARAM, required = false, defaultValue = DEFAULT_NUMBER_OF_USERS_PARAM_VALUE) final Integer numberOfUsers
    ) {
        LOGGER.debug("Receive request to get {} users for gRPC stream server call", numberOfUsers);

        userGrpcStreamClient.get(numberOfUsers);

        LOGGER.debug("Finis gRPC stream server call processing");

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
