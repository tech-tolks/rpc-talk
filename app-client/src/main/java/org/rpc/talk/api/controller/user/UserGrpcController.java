package org.rpc.talk.api.controller.user;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.client.user.UserGrpcClient;
import org.rpc.talk.domain.ResponseStat;
import org.rpc.talk.domain.UserDto;
import org.rpc.talk.service.UserGenerator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.rpc.talk.api.constant.SwaggerConstants.OK_STATUS;
import static org.rpc.talk.api.constant.SwaggerConstants.SERVER_ERROR;
import static org.rpc.talk.api.constant.SwaggerConstants.SERVER_ERROR_STATUS;
import static org.rpc.talk.api.constant.SwaggerConstants.SUCCESSFUL_OPERATION;
import static org.rpc.talk.api.constant.UserConstants.DEFAULT_NUMBER_OF_USERS_PARAM_VALUE;
import static org.rpc.talk.api.constant.UserConstants.NUMBER_OF_USERS_PARAM;
import static org.rpc.talk.api.constant.UserConstants.USER_ENDPOINT;


@RestController
@RequestMapping(value = USER_ENDPOINT)
@Tag(name = "Users")
public class UserGrpcController {

    private static final Logger LOGGER = LogManager.getLogger(UserGrpcController.class);

    private final UserGrpcClient userClient;
    private final UserGenerator userGenerator;


    public UserGrpcController(final UserGrpcClient userClient,
                              final UserGenerator userGenerator) {
        this.userClient = userClient;
        this.userGenerator = userGenerator;
    }


    @Operation(
            tags = {"grpc", "add users"},
            method = "GET",
            parameters = {
                    @Parameter(name = NUMBER_OF_USERS_PARAM, in = ParameterIn.QUERY,
                            content = @Content(schema = @Schema(implementation = String.class, defaultValue = DEFAULT_NUMBER_OF_USERS_PARAM_VALUE)))
            },
            responses = {
                    @ApiResponse(responseCode = OK_STATUS, description = SUCCESSFUL_OPERATION,
                            content = @Content(schema = @Schema(implementation = ResponseStat.class))),
                    @ApiResponse(responseCode = SERVER_ERROR_STATUS, description = SERVER_ERROR,
                            content = @Content(schema = @Schema(implementation = Object.class)))
            }
    )
    @GetMapping(value = "/grpc/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseStat<Integer>> add(
            @RequestParam(value = NUMBER_OF_USERS_PARAM, required = false, defaultValue = DEFAULT_NUMBER_OF_USERS_PARAM_VALUE) final Integer numberOfUsers
    ) {
        LOGGER.debug("Receive request to add {} users for gRPC server call", numberOfUsers);

        final List<UserDto> users = userGenerator.generateUsers(numberOfUsers);
        final ResponseStat<Integer> responseStat = userClient.add(users);

        LOGGER.debug("Response from gRPC client: {}", responseStat);

        return new ResponseEntity<>(responseStat, HttpStatus.OK);
    }
}
