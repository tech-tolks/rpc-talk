package org.rpc.talk.api.controller.greeting;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.client.greeting.GreetingRestClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.rpc.talk.api.constant.GreetingConstants.DEFAULT_NAME_PARAM_VALUE;
import static org.rpc.talk.api.constant.GreetingConstants.GREETING_ENDPOINT;
import static org.rpc.talk.api.constant.GreetingConstants.NAME_PARAM;
import static org.rpc.talk.api.constant.SwaggerConstants.OK_STATUS;
import static org.rpc.talk.api.constant.SwaggerConstants.SERVER_ERROR;
import static org.rpc.talk.api.constant.SwaggerConstants.SERVER_ERROR_STATUS;
import static org.rpc.talk.api.constant.SwaggerConstants.SUCCESSFUL_OPERATION;


@RestController
@RequestMapping(value = GREETING_ENDPOINT)
@Tag(name = "Greeting")
public class GreetingRestController {

    private static final Logger LOGGER = LogManager.getLogger(GreetingRestController.class);

    private final GreetingRestClient greetingClient;


    public GreetingRestController(final GreetingRestClient greetingClient) {
        this.greetingClient = greetingClient;
    }


    @Operation(
            tags = {"rest", "greeting"},
            method = "GET",
            parameters = {
                    @Parameter(name = NAME_PARAM, in = ParameterIn.QUERY,
                            content = @Content(schema = @Schema(implementation = String.class, defaultValue = DEFAULT_NAME_PARAM_VALUE)))
            },
            responses = {
                    @ApiResponse(responseCode = OK_STATUS, description = SUCCESSFUL_OPERATION,
                            content = @Content(schema = @Schema(implementation = String.class))),
                    @ApiResponse(responseCode = SERVER_ERROR_STATUS, description = SERVER_ERROR,
                            content = @Content(schema = @Schema(implementation = Object.class)))
            }
    )
    @GetMapping(value = "/rest", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> greet(
            @RequestParam(value = NAME_PARAM, required = false, defaultValue = DEFAULT_NAME_PARAM_VALUE) final String name
    ) {
        LOGGER.debug("Receive greeting request for REST server call. name param: {}", name);

        final String greeting = greetingClient.greet(name);

        LOGGER.debug("Greeting response from REST client: {}", greeting);

        return new ResponseEntity<>(greeting, HttpStatus.OK);
    }
}
