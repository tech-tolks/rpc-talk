package org.rpc.talk.api.constant;

public class GreetingConstants {
    public static final String GREETING_ENDPOINT = "/greeting";
    public static final String NAME_PARAM = "name";
    public static final String DEFAULT_NAME_PARAM_VALUE = "Stranger";


    private GreetingConstants() {
    }
}
