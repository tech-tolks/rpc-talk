package org.rpc.talk.api.constant;

public class UserConstants {
    public static final String USER_ENDPOINT = "/user";
    public static final String NUMBER_OF_USERS_PARAM = "numberOfUsers";
    public static final String DEFAULT_NUMBER_OF_USERS_PARAM_VALUE = "10";


    private UserConstants() {
    }
}
