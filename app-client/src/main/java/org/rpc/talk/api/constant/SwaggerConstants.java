package org.rpc.talk.api.constant;

public class SwaggerConstants {
    public static final String SUCCESSFUL_OPERATION = "Successful Operation";
    public static final String SERVER_ERROR = "Server Error";

    public static final String OK_STATUS = "200";
    public static final String SERVER_ERROR_STATUS = "500";


    private SwaggerConstants() {
    }
}
