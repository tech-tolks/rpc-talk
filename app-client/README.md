# app-client

## About

Project contains REST api, gRPC and REST clients for app-server.

## Project requirements

- Java 8

## How to build

```shell
./mvnw clean install
```

NOTE: you should build [app-root](../app-root/README.md) and [app-core](../app-core/README.md) before.

## How to run locally?

```shell
java -jar target/app-client.jar
```

app-client starts on port 8082

NOTE: app-server should be started with the following (default for app-server) ports binding:

- gRPC endpoint - 8080
- REST endpoint - 8081

#### Override ports binding

```shell
java -jar target/app-client.jar --server.port=app_client_port --app.server.grpc.port=app_server_grpc_port --app.server.rest.port=app_server_rest_port
```

Where:

- app_client_port - port for REST app-client REST endpoints
- app_server_grpc_port - app-server port bound for gRPC endpoint
- app_server_rest_port - app-server port bound for REST endpoint

Example:

```shell
java -jar target/app-client.jar --server.port=7777 --app.server.grpc.port=8888 --app.server.rest.port=9999
```

## Swagger

- UI - [http://localhost:8082/swagger-ui/index.html](http://localhost:8082/swagger-ui/index.html)
- JSON - [http://localhost:8082/v3/api-docs/](http://localhost:8082/v3/api-docs/)

Change port number if you use different from the default