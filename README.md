## Project description

- [app-root](app-root/README.md) -
  contains [BOM](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html#bill-of-materials-bom-poms)
  file for services.

- [app-core](app-core/README.md) - lib with common classes
  and [proto](https://developers.google.com/protocol-buffers/docs/overview) files for app-client and app-server

- [app-client](app-client/README.md) - gRPC client for app-server

- [app-server](app-server/README.md) - gRPC/REST server

- [documentation](documents/README.md) - contains presentation and other useful documents

- scripts - useful scripts

## How to build

```shell
. scripts/build.sh
```

The following command will build:

- app-root
- app-core
- app-client
- app-server

## How to run

### Using Docker Compose

Pre-requirements

- [Install Docker Compose](https://docs.docker.com/compose/install/#install-compose)

Use the following command to run app-client and app-server

```shell
docker-compose up
```

app-client REST endpoint starts on port **8082**

app-server gRPC and REST endpoints starts on ports **8080** and **8081** accordingly.

To change ports bindings you can use ENV variables

```shell
APP_SERVER_GRPC_PORT=YOUR_APP_SERVER_GRPC_PORT APP_SERVER_REST_PORT=YOUR_APP_SERVER_REST_PORT APP_CLIENT_PORT=YOUR_APP_CLIENT_PORT docker-compose up
```

Where:

- APP_SERVER_GRPC_PORT - port for server gRPC endpoint
- APP_SERVER_REST_PORT - port for server REST endpoint
- APP_CLIENT_PORT - port for client REST endpoint

Example:

```shell
APP_SERVER_GRPC_PORT=7777 APP_SERVER_REST_PORT=8888 APP_CLIENT_PORT=9999 docker-compose up
```

### Using scripts (IN PROGRESS)

#### Run app-server:

```shell
. scripts/run-server.sh
```

app-server starts on port **8080**

#### Run app-client:

```shell
. scripts/run-client.sh
```

app-client starts on port **8082**

## How to use

1. Import [postman collection](./documents/postman/README.md)
2. Follow ["Showcase"](./documents/usage/README.md) document to perform needed actions

## Useful links

gRPC

- [Documentation](https://grpc.io/docs/)
- [Introduction to gRPC](https://grpc.io/docs/what-is-grpc/introduction/)
- [Protocol buffer documentation](https://developers.google.com/protocol-buffers/docs/overview)

RPC

- [wiki](https://en.wikipedia.org/wiki/Remote_procedure_call)