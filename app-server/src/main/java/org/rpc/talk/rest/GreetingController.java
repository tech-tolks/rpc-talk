package org.rpc.talk.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/greet")
public class GreetingController {

    private static final Logger LOGGER = LogManager.getLogger(GreetingController.class);

    private static final String GREETING = "Hello, %s";


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> greet(
            @RequestParam(value = "name", required = false, defaultValue = "Stranger") final String name
    ) {
        LOGGER.debug("Receive greeting REST request. name param: {}", name);

        final ResponseEntity<String> response = new ResponseEntity<>(String.format(GREETING, name), HttpStatus.OK);

        LOGGER.debug("Greeting REST response: {}", response);

        return response;
    }
}
