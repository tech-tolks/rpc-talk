package org.rpc.talk.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.domain.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(value = "/user")
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);


    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> add(@RequestBody final List<UserDto> users) {
        LOGGER.debug("Receive REST request to add {} users", users.size());

        final ResponseEntity<Integer> response = new ResponseEntity<>(users.size(), HttpStatus.OK);

        LOGGER.debug("Add users REST response: {}", response);

        return response;
    }
}
