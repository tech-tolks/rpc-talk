package org.rpc.talk;

import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.io.IOException;
import java.util.List;


@SpringBootApplication
@ComponentScan(basePackages = "org.rpc.talk.*")
public class Application implements CommandLineRunner {

    private final List<BindableService> services;
    private final Integer serverPort;


    public Application(final List<BindableService> services,
                       @Value("${app.grpc.server.port}") final Integer serverPort) {
        this.services = services;
        this.serverPort = serverPort;
    }


    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Override
    public void run(final String... args) throws IOException, InterruptedException {
        final ServerBuilder serverBuilder = ServerBuilder.forPort(serverPort);
        services.forEach(serverBuilder::addService);

        final Server server = serverBuilder
                .maxInboundMessageSize(320000000) // TODO move to property
                .build();

        server.start();
        server.awaitTermination();
    }
}
