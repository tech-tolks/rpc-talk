package org.rpc.talk.grpc;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.grpc.service.AddUsersResponse;
import org.rpc.talk.grpc.service.GetUsersRequest;
import org.rpc.talk.grpc.service.User;
import org.rpc.talk.grpc.service.UserStreamServiceGrpc;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;


@Service
public class UserStreamServiceGrpcImpl extends UserStreamServiceGrpc.UserStreamServiceImplBase {

    private static final Logger LOGGER = LogManager.getLogger(UserStreamServiceGrpcImpl.class);

    private final List<User> users = new ArrayList<>();


    @Override
    public StreamObserver<User> add(final StreamObserver<AddUsersResponse> responseObserver) {
        return new StreamObserver<User>() {
            /**
             * When receive new user from stream
             * @param user to add
             */
            @Override
            public void onNext(final User user) {
                users.add(user);
                LOGGER.debug("New user with name {} added", user.getFirstName());
            }


            /**
             * When stream is complete return response with total number of users
             */
            @Override
            public void onCompleted() {
                final AddUsersResponse response = AddUsersResponse.newBuilder()
                        .setNumberOfUsers(users.size())
                        .build();

                LOGGER.debug("Add users response: {}", response);

                responseObserver.onNext(response);
                responseObserver.onCompleted();
            }


            /**
             * Errors handling
             *
             * @param throwable exception
             */
            @Override
            public void onError(final Throwable throwable) {
                LOGGER.error("Unable to add user. Process failed with status: {}", Status.fromThrowable(throwable));
            }
        };
    }


    @Override
    public StreamObserver<User> addAndReceive(final StreamObserver<AddUsersResponse> responseObserver) {
        return new StreamObserver<User>() {
            /**
             * When receive new user from stream
             * @param user to add
             */
            @Override
            public void onNext(final User user) {
                try {
                    Thread.sleep(200);
                    users.add(user);
                    LOGGER.debug("New user with name {} added", user.getFirstName());
                    responseObserver.onNext(
                            AddUsersResponse.newBuilder()
                                    .setNumberOfUsers(users.size())
                                    .build()
                    );
                } catch (InterruptedException e) {
                    throw new RuntimeException();
                }
            }


            /**
             * Complete stream
             */
            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }


            /**
             * Errors handling
             * @param throwable exception
             */
            @Override
            public void onError(final Throwable throwable) {
                LOGGER.error("Unable to add user. Process failed with status: {}", Status.fromThrowable(throwable));
            }
        };
    }


    @Override
    public void get(final GetUsersRequest request, final StreamObserver<User> responseObserver) {
        getUsers(request.getNumberOfUsers())
                .peek(user -> LOGGER.debug("Send user with name {} to client", user.getFirstName()))
                .forEach(responseObserver::onNext);

        responseObserver.onCompleted();
    }


    private Stream<User> getUsers(final int numberOfUsers) {
        if (numberOfUsers >= users.size()) {
            return users.stream();
        }

        return IntStream.rangeClosed(0, numberOfUsers)
                .mapToObj(users::get);
    }
}
