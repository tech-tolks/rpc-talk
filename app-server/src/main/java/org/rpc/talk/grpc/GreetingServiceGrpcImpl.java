package org.rpc.talk.grpc;

import io.grpc.stub.StreamObserver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.grpc.service.GreetingRequest;
import org.rpc.talk.grpc.service.GreetingResponse;
import org.rpc.talk.grpc.service.GreetingServiceGrpc;
import org.springframework.stereotype.Service;


@Service
public class GreetingServiceGrpcImpl extends GreetingServiceGrpc.GreetingServiceImplBase {

    private static final Logger LOGGER = LogManager.getLogger(GreetingServiceGrpcImpl.class);

    private static final String GREETING = "Hello, %s";


    @Override
    public void hello(final GreetingRequest request, final StreamObserver<GreetingResponse> responseObserver) {
        LOGGER.debug("Receive greeting gRPC request. name param: {}", request.getName());

        final GreetingResponse response = GreetingResponse.newBuilder()
                .setGreeting(String.format(GREETING, request.getName()))
                .build();

        LOGGER.debug("Greeting gRPC response: {}", response);

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
