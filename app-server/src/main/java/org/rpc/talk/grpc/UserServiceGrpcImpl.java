package org.rpc.talk.grpc;

import io.grpc.stub.StreamObserver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rpc.talk.grpc.service.AddUsersRequest;
import org.rpc.talk.grpc.service.AddUsersResponse;
import org.rpc.talk.grpc.service.UserServiceGrpc;
import org.springframework.stereotype.Service;


@Service
public class UserServiceGrpcImpl extends UserServiceGrpc.UserServiceImplBase {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceGrpcImpl.class);


    @Override
    public void add(final AddUsersRequest request, final StreamObserver<AddUsersResponse> responseObserver) {
        LOGGER.debug("Receive gRPC request to add {} users", request.getUsersCount());

        final AddUsersResponse response = AddUsersResponse.newBuilder()
                .setNumberOfUsers(request.getUsersCount())
                .build();

        LOGGER.debug("Add users gRPC response: {}", response);

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
