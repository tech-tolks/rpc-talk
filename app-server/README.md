# app-server

## About

Project contains gRPC and REST endpoints.

## Project requirements

- Java 8

## How to build

```shell
./mvnw clean install
```

NOTE: you should build [app-root](../app-root/README.md) and [app-core](../app-core/README.md) before.

## How to run locally?

```shell
java -jar target/app-server.jar
```

app-server starts on port **8080** and **8081** ports for gRPC and REST accordingly.

#### Override ports binding

```shell
java -jar target/app-server.jar --app.grpc.server.port=grpc_port --server.port=rest_port
```

Where:

- grpc_port - port for gRPC endpoints
- rest_port - port for REST endpoints

Example:

```shell
java -jar target/app-server.jar --app.grpc.server.port=7777 --server.port=8888
```